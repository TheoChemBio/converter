## Amber to Gromacs

Convert AMBER input files (prmcrd, prmtop) to GROMACS format (gro, top)

+ Usage:

    ```
    perl amber_to_gromacs.pl -top mol.prmtop -crd mol.prmcrd -name mol
    ```

+ Supports glycam diheral parameters via ``-glycam yes`` option.

