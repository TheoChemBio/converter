## Convert general contraction

Convert EMSL basis set format to generally contracted format

+ Usage:

    ```
    python convert-gc.py cc-pvdz
    ```
