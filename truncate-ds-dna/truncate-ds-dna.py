#!/usr/bin/env python

import __future__
import math
import sys

class Atom(object):

    def __init__(self, name, res_id, res_name, coord):
        self.name = name
        self.res_id = res_id
        self.res_name = res_name
        self.coord = coord

    def __str__(self):
        return "%s %s" % (self.name, self.coord)

    def dist(self, ref_atom):
        xyz0 = [float(a) for a in ref_atom.coord.split()]
        xyz1 = [float(a) for a in self.coord.split()]

        return math.sqrt((xyz1[0]-xyz0[0])**2 +
                         (xyz1[1]-xyz0[1])**2 +
                         (xyz1[2]-xyz0[2])**2)

    def scale(self, ref_atom, ref_dist):
        xyz0 = [float(a) for a in ref_atom.coord.split()]
        xyz1 = [float(a) for a in self.coord.split()]

        dist = math.sqrt((xyz1[0]-xyz0[0])**2 +
                         (xyz1[1]-xyz0[1])**2 +
                         (xyz1[2]-xyz0[2])**2)

        xyz1[0] = xyz0[0] + (xyz1[0]-xyz0[0]) * ref_dist / dist
        xyz1[1] = xyz0[1] + (xyz1[1]-xyz0[1]) * ref_dist / dist
        xyz1[2] = xyz0[2] + (xyz1[2]-xyz0[2]) * ref_dist / dist

        self.coord = "%8.3f%8.3f%8.3f" % (xyz1[0], xyz1[1], xyz1[2])

# ==> collect atoms <==

if len(sys.argv) <= 1:
    print("Need input pdb file!")
    exit()

f_pdb = open(sys.argv[1], "r")

atoms = []

for line in f_pdb:
    if line[0:4] == "ATOM" or line[0:6] == "HETATM":
        name = line[12:16].split()[0]
        res_id = int(line[22:26])
        res_name = line[17:20].split()[0]
        coord = line[30:54]
        if res_name in ["DG","DC","DA","DT"]:
            atoms.append(Atom(name, res_id, res_name, coord))

f_pdb.close()

# ==> collect residue id <==

res_ids = []

for atom in atoms:
    if not atom.res_id in res_ids:
        res_ids.append(atom.res_id)

print("Found %d DNA bases." % len(res_ids))

# ==> process DNA bases <==

res_atoms = dict()

for res_id in res_ids:
    res_atoms[res_id] = []
    for atom in atoms:
        if atom.res_id == res_id:
            # cut the sugar unit
            if atom.name in ["C1'","H1'","O4'","C2'"]:
                res_atoms[res_id].append(atom)
            elif not ("T" in atom.name or "P" in atom.name or "'" in atom.name):
                res_atoms[res_id].append(atom)

    for i,atom in enumerate(res_atoms[res_id]):
        if atom.name == "C1'": a0 = i
        if atom.name == "H1'": a1 = i
        if atom.name == "C2'": a2 = i
        if atom.name == "O4'": a3 = i

    # replace dangling atoms by H
    ref_dist = res_atoms[res_id][a1].dist(res_atoms[res_id][a0])

    res_atoms[res_id][a2].scale(res_atoms[res_id][a0], ref_dist)
    res_atoms[res_id][a3].scale(res_atoms[res_id][a0], ref_dist)

    res_atoms[res_id][a2].name = "H2'"
    res_atoms[res_id][a3].name = "H3'"

# ==> process DNA base pairs <==

str_xyz = ""
num_xyz = 0

frag_info = []

for res_id_1 in res_ids:
    for res_id_2 in res_ids:
        if res_id_1 >= res_id_2:
            continue
        # find minimal distance between two bases
        min_dist = 1.0e+99
        for atom_1 in res_atoms[res_id_1]:
            for atom_2 in res_atoms[res_id_2]:
                dist = atom_1.dist(atom_2)
                if dist < min_dist:
                    min_dist = dist
        # paired bases should have minimal distance < 2.0 Angstrom
        if min_dist < 2.0:
            for atom in res_atoms[res_id_1]:
                atom.name = "".join([s for s in atom.name if s.isalpha()])
                str_xyz += ("%s\n" % atom)
                num_xyz += 1
            for atom in res_atoms[res_id_2]:
                atom.name = "".join([s for s in atom.name if s.isalpha()])
                str_xyz += ("%s\n" % atom)
                num_xyz += 1
            frag_info.append("%s  0  1\n" % (
                len(res_atoms[res_id_1]) + len(res_atoms[res_id_2])
                ))

print("Found %d DNA base pairs." % len(frag_info))

if len(frag_info) * 2 != len(res_ids):
    print("WARNING: Number of DNA bases and pairs do not match!")

# ==> write xyz file and frag info <==

f_xyz = open("truncate-ds-dna.xyz", "w")
f_xyz.write("%d\n\n%s" % (num_xyz, str_xyz))
f_xyz.close()

f_frag = open("frag.info", "w")
f_frag.write("%d\n%s" % (len(frag_info), "".join(frag_info)))
f_frag.close()

print("Truncated DNA bases written to file truncate-ds-dna.xyz and frag.info.")
