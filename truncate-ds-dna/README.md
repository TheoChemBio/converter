## Truncate double-strand DNA

Convert double-strand DNA (pdb format) to truncated base pairs (xyz format).

Usage:

1. Download 1BNA.pdb

    ```
    wget https://files.rcsb.org/download/1BNA.pdb
    ```

2. Add hydrogen using e.g. Gromacs

    ```
    pdb2gmx -f 1BNA.pdb
    ```

3. Run truncate-ds-dna.py on the output pdb of Step 2

    ```
    python truncate-ds-dna.py conf.pdb
    ```
