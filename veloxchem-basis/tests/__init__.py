import pathlib
import sys

p = pathlib.Path(__file__)
sys.path.append(str(p.parent.parent))
import convert_vlx_basis
