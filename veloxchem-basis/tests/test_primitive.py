from convert_vlx_basis import Primitive, check_same_exp


def test_primitive():
    p = Primitive('s', "1.0", "0.1")
    assert p.e == "1.0"
    assert p.c == "0.1"


def test_estring():
    p = Primitive('s', "1.0", "0.1")
    assert p.e_string() == "1.000000000000e+00"


def test_cstring():
    p = Primitive('s', "1.0", "0.1")
    assert p.c_string() == "  1.000000000000e-01"


def test_check():
    shell1 = [Primitive('s', '1.0', '0.1'), Primitive('p', '2.0', '.01')]
    shell2 = [Primitive('s', '.50', '0.05'), Primitive('p', '2.0', '.01')]
    assert check_same_exp(shell1, shell1)
    assert not check_same_exp(shell1, shell2)
