import pathlib
import os
import sys
import shutil
import pytest

from convert_vlx_basis import main

vlx_root = pathlib.Path(__file__).parent.parent

# Test that the reference output files in the reference directory
# are reproduced by the script.

reference = vlx_root/'reference'
cases = [pathlib.Path(f.name) for f in reference.glob('*')]
names = [case.name for case in cases]


@pytest.mark.parametrize('case', cases, ids=names)
def test_acceptance(case, tmp_path):
    """
    Vefify that generated basis files match files in ./reference
    """
    arg = case.name.lower()

    emsl = f'{arg}.emsl.bs'
    shutil.copyfile(vlx_root/emsl, tmp_path/emsl)

    # Run the full program in tmpdir
    os.chdir(tmp_path)
    sys.argv[1:] = [arg]
    main()

    assert open(case).read() == open(reference/case).read()

    case.unlink()
