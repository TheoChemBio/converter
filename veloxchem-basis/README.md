## VeloxChem basis set

Convert EMSL basis set format to VeloxChem format

+ Usage:

    ```
    python convert-vlx-basis.py def2-svp
    ```
