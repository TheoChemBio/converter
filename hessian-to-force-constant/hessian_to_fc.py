#!/usr/bin/env python3

"""
              ==== HESSIAN >>> to >> FC ====
Convert Hessian to bond stretching and angle bending parameters.
* Hessian is read from Gaussian fchk file.
* Output parameters are in GROMACS format.

Copyright (C) 2019, Xin Li

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

Usage: ./hessian_to_fc.py xxx.fchk [2-3 atoms]
Ref: JM Seminario, Int. J. Quantum Chem., 1996, 60, 1271-1277.
"""

import sys
from os.path import isfile
import numpy as np
import math


def main():

    if len(sys.argv) <= 1:
        print("Usage: ./hessian_to_fc.py xxx.fchk [2-3 atoms]")
        return

    # Gaussian fchk file name

    fchk_file = sys.argv[1]
    if not isfile(fchk_file):
        print("File {:s} doesn't exist!".format(fchk_file))
        return

    # atom indices and job type (bond/angle)

    if len(sys.argv) == 4:
        ai = int(sys.argv[2]) - 1
        aj = int(sys.argv[3]) - 1
        jobtype = "BOND"

    elif len(sys.argv) == 5:
        ai = int(sys.argv[2]) - 1
        aj = int(sys.argv[3]) - 1
        ak = int(sys.argv[4]) - 1
        jobtype = "ANGLE"

    else:
        print("Please specify 2-3 atoms!")
        return

    # read fchk file for coordinates and Hessian

    coords, hessian = read_fchk(fchk_file)

    # calculate force constant

    if jobtype == "BOND":
        r0, kbond_ij = compute_bond(coords, hessian, ai, aj)
        r0, kbond_ji = compute_bond(coords, hessian, aj, ai)
        kbond = 0.5 * (kbond_ij + kbond_ji)
        print("{:-8d}{:-8d}{:-8d}{:15.6e}{:15.6e}".format(
            ai + 1, aj + 1, 1, r0, kbond))

    if jobtype == "ANGLE":
        theta, kangle_ijk = compute_angle(coords, hessian, ai, aj, ak)
        theta, kangle_kji = compute_angle(coords, hessian, ak, aj, ai)
        kangle = 0.5 * (kangle_ijk + kangle_kji)
        print("{:-7d}{:-7d}{:-7d}{:-7d}{:15.6e}{:15.6e}".format(
            ai + 1, aj + 1, ak + 1, 1, theta, kangle))


def read_fchk(fchk_file):
    """Reads fchk file for coordinates and Hessian."""

    coords_list = []
    hessian_list = []

    with open(fchk_file, 'r') as f_hessian:

        while (True):
            line = f_hessian.readline()
            if not line:
                break

            if "Current cartesian coordinates" in line:
                ndf = int(line.split()[5])
                assert ndf % 3 == 0
                natoms = ndf // 3
                while (True):
                    line = f_hessian.readline()
                    coords_list += [float(x) for x in line.split()]
                    if len(coords_list) == ndf:
                        break

            if "Cartesian Force Constants" in line:
                while (True):
                    line = f_hessian.readline()
                    hessian_list += [float(x) for x in line.split()]
                    if len(hessian_list) == ndf * (ndf + 1) // 2:
                        break

    assert len(coords_list) == ndf
    assert len(hessian_list) == ndf * (ndf + 1) // 2

    # get coordinates as numpy array

    coords = np.array(coords_list).reshape((natoms, 3))

    # get full Hessian as numpy array

    hessian = np.zeros((ndf, ndf))

    index = 0
    for i in range(ndf):
        for j in range(i + 1):
            hessian[i, j] = hessian_list[index]
            hessian[j, i] = hessian_list[index]
            index += 1

    assert index == ndf * (ndf + 1) // 2

    return coords, hessian


def compute_bond(coords, hessian, ai, aj):
    """Calculates bond stretching parameter."""

    vec_ij = coords[aj, :] - coords[ai, :]
    r_ij = np.linalg.norm(vec_ij)
    vec_ij /= r_ij

    r0 = r_ij
    r0 *= (0.52917721092 * 0.1)  # nm

    i0, i1 = ai * 3, (ai + 1) * 3
    j0, j1 = aj * 3, (aj + 1) * 3
    hessian_AB = -hessian[i0:i1, j0:j1]

    eigvals, eigvecs = np.linalg.eig(hessian_AB)

    kbond = 0.0
    for d in range(3):
        kbond += eigvals[d] * abs(np.dot(vec_ij, eigvecs[:, d]))
    kbond = np.real(kbond)

    kbond *= 6.02214129 * 4.35974434 * 100.0  # kJ mol^-1 Bohr^-2
    kbond /= (0.52917721092 * 0.1)**2  # kJ mol^-1 nm^-2

    return r0, kbond


def compute_angle(coords, hessian, ai, aj, ak):
    """Calculates angle bending parameter."""

    vec_ij = coords[aj, :] - coords[ai, :]
    r_ij = np.linalg.norm(vec_ij)
    vec_ij /= r_ij

    vec_kj = coords[aj, :] - coords[ak, :]
    r_kj = np.linalg.norm(vec_kj)
    vec_kj /= r_kj

    theta = math.acos(np.dot(vec_ij, vec_kj)) / math.pi * 180.0  # degree

    vec_N = np.cross(vec_kj, vec_ij)
    vec_N /= np.linalg.norm(vec_N)

    vec_PA = np.cross(vec_N, vec_ij)
    vec_PC = np.cross(vec_kj, vec_N)

    i0, i1 = ai * 3, (ai + 1) * 3
    j0, j1 = aj * 3, (aj + 1) * 3
    k0, k1 = ak * 3, (ak + 1) * 3
    hessian_AB = -hessian[i0:i1, j0:j1]
    hessian_CB = -hessian[k0:k1, j0:j1]

    eigvals_AB, eigvecs_AB = np.linalg.eig(hessian_AB)
    eigvals_CB, eigvecs_CB = np.linalg.eig(hessian_CB)

    k1 = 0.0
    for d in range(3):
        k1 += eigvals_AB[d] * abs(np.dot(vec_PA, eigvecs_AB[:, d]))
    k1 = np.real(k1)

    k2 = 0.0
    for d in range(3):
        k2 += eigvals_CB[d] * abs(np.dot(vec_PC, eigvecs_CB[:, d]))
    k2 = np.real(k2)

    kangle = 1.0 / (1.0 / (r_ij**2 * k1) + 1.0 / (r_kj**2 * k2))
    kangle *= 6.02214129 * 4.35974434 * 100.0  # kJ mol^-1

    return theta, kangle


if __name__ == "__main__":
    main()
