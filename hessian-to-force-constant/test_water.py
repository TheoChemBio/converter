#!/usr/bin/env python3

from hessian_to_fc import compute_bond
from hessian_to_fc import compute_angle

import numpy as np
import unittest


class TestHessianToFC(unittest.TestCase):

    def test_water(self):

        coords_str = """
          1.07852077e-32  4.12136467e-33  2.19841322e-01 -2.95822839e-31  1.43745479e+00
         -8.79365290e-01 -1.76037441e-16 -1.43745479e+00 -8.79365290e-01
        """

        hessian_str = """
          1.60974310e-04 -4.24203827e-12  7.16548122e-01 -2.16478357e-11 -2.58523368e-11
          4.65767738e-01 -8.04871654e-05  1.11153936e-11  3.13684629e-11  7.54889510e-05
         -1.81519625e-11 -3.58274061e-01  2.14984232e-01  9.96020317e-12  3.87213653e-01
          5.57914044e-12  2.74024652e-01 -2.32883869e-01 -1.16852044e-11 -2.44504442e-01
          2.21051399e-01 -8.04871520e-05  2.68733883e-11 -3.41864182e-11  4.99821114e-06
         -2.49141514e-11  3.07104322e-11  7.54889510e-05  1.43139797e-11 -3.58274061e-01
         -2.14984232e-01 -2.49141242e-11 -2.89395922e-02 -2.95202096e-02  9.95931152e-12
          3.87213653e-01  1.88872632e-11 -2.74024652e-01 -2.32883869e-01 -3.07104065e-11
          2.95202096e-02  1.18324704e-02  1.16845660e-11  2.44504442e-01  2.21051399e-01
        """

        coords_list = [float(x) for x in coords_str.split()]
        natoms = len(coords_list) // 3
        coords = np.array(coords_list).reshape(natoms, 3)

        hessian_list = [float(x) for x in hessian_str.split()]
        hessian = np.zeros((natoms * 3, natoms * 3))
        index = 0
        for i in range(natoms * 3):
            for j in range(i + 1):
                hessian[i, j] = hessian_list[index]
                hessian[j, i] = hessian_list[index]
                index += 1

        r0, kbond = compute_bond(coords, hessian, 0, 1)
        self.assertAlmostEqual(r0, 0.095758, 6)
        self.assertAlmostEqual(kbond, 514247.11, 2)

        r0, kbond = compute_bond(coords, hessian, 1, 0)
        self.assertAlmostEqual(r0, 0.095758, 6)
        self.assertAlmostEqual(kbond, 513230.54, 2)

        r0, kbond = compute_bond(coords, hessian, 0, 2)
        self.assertAlmostEqual(r0, 0.095758, 6)
        self.assertAlmostEqual(kbond, 514247.11, 2)

        r0, kbond = compute_bond(coords, hessian, 2, 0)
        self.assertAlmostEqual(r0, 0.095758, 6)
        self.assertAlmostEqual(kbond, 513230.54, 2)

        theta, kangle = compute_angle(coords, hessian, 1, 0, 2)
        self.assertAlmostEqual(theta, 105.190, 3)
        self.assertAlmostEqual(kangle, 346.82542, 5)

        theta, kangle = compute_angle(coords, hessian, 2, 0, 1)
        self.assertAlmostEqual(theta, 105.190, 3)
        self.assertAlmostEqual(kangle, 346.82542, 5)


if __name__ == "__main__":
    unittest.main()
